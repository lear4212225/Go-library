module gitlab.com/lear4212225/Go-library

go 1.20

require github.com/joho/godotenv v1.5.1

require (
	github.com/brianvoe/gofakeit v3.18.0+incompatible // indirect
	github.com/brianvoe/gofakeit/v6 v6.22.0 // indirect
	github.com/go-chi/chi/v5 v5.0.8 // indirect
	github.com/jackc/pgpassfile v1.0.0 // indirect
	github.com/jackc/pgservicefile v0.0.0-20221227161230-091c0ba34f0a // indirect
	github.com/jackc/pgx/v5 v5.4.1 // indirect
	github.com/jackc/puddle/v2 v2.2.0 // indirect
	golang.org/x/crypto v0.9.0 // indirect
	golang.org/x/sync v0.1.0 // indirect
	golang.org/x/text v0.9.0 // indirect
)
