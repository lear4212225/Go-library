package main

import (
	"gitlab.com/lear4212225/Go-library/internal/app"
	"gitlab.com/lear4212225/Go-library/pkg/config"
	"log"
)

func main() {
	cfg, err := config.NewConfig()
	if err != nil {
		log.Fatal(err)
	}
	err = app.Run(cfg)
	if err != nil {
		log.Fatal(err)
	}
}
