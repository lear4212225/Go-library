//nolint:all
package public

import (
	"gitlab.com/lear4212225/Go-library/internal/model"
)

//go:generate swagger generate spec -o ./swagger.json --scan-models

// swagger:route GET /author/show-all getAllAuthor
// Show all authors.
// responses:
//   200: searchVacancyByQueryResponse

// swagger:parameters getAllAuthor
type getAllAuthor struct {
}

// swagger:response searchVacancyByQueryResponse
type searchVacancyByQueryResponse struct {
	//  in:body
	Body []model.Author
}

// swagger:route POST /author/add addAuthor
// Add author to database
// responses:
//   200: addAuthorResponse

// swagger:parameters addAuthor
type addAuthor struct {
	// Required: true
	//  in:body
	Body model.Author
}

// swagger:response addAuthorResponse
type addAuthorResponse struct {
}

// swagger:route GET /book/show showAllBooks
// Show all authors.
// responses:
//   200: searchVacancyByQueryResponse

// swagger:parameters showAllBooks
type showAllBooks struct {
}

// swagger:response showAllBooksResponse
type showAllBooksResponse struct {
	//  in:body
	Body []model.Book
}

// swagger:route POST /book/add addBook
// Add book to database.
// responses:
//   200: addBookResponse

// swagger:parameters addBook
type addBook struct {
	// Required: true
	//  in:body
	Body model.Book
}

// swagger:response addBookResponse
type addBookResponse struct {
}

// swagger:route GET /user/show showAllUsers
// Show all authors.
// responses:
//   200: showAllUsersResponse

// swagger:parameters showAllUsers
type showAllUsers struct {
}

// swagger:response showAllUsersResponse
type showAllUsersResponse struct {
	//  in:body
	Body []model.User
}

// swagger:route POST /user/give-book userGiveBook
// Add book to database
// responses:
//   200: userGiveBookResponse

type rentBook struct {
	BookID int `json:"book_id"`
	UserID int `json:"user_id"`
}

// swagger:parameters userGiveBook
type userGiveBook struct {
	// Required: true
	//  in:body
	Body rentBook
}

// swagger:response userGiveBookResponse
type userGiveBookResponse struct {
}

// swagger:route POST /user/give-back-book userBackGiveBook
// Add book to database
// responses:
//   200: userGiveBookResponse

type giveBackRentBook struct {
	UserID int `json:"user_id"`
}

// swagger:parameters userBackGiveBook
type userBackGiveBook struct {
	// Required: true
	//  in:body
	Body giveBackRentBook
}

// swagger:response userGiveBackBookResponse
type userGiveBackBookResponse struct {
}
