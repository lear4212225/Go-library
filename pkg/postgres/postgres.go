package postgres

import (
	"context"
	"github.com/jackc/pgx/v5/pgxpool"
	"gitlab.com/lear4212225/Go-library/pkg/config"
	"time"
)

func DTBConn(cfg *config.Config) (*pgxpool.Pool, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	conn, err := pgxpool.New(ctx, "postgres://habrpguser:pgpwd4habr@127.0.0.1:5432/habrdb")
	if err != nil {
		return nil, err
	}

	err = conn.Ping(ctx)
	if err != nil {
		return nil, err
	}
	return conn, nil

}
