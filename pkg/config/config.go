package config

import (
	"github.com/joho/godotenv"
	"log"
	"os"
)

type Config struct {
	DB     string
	DbName string
	DbUser string
	DbPass string
	DbPort string
	DbHost string
	Addr   string
}

// GetConf returns a new Config struct
func NewConfig() (*Config, error) {
	// Store the PATH environment variable in a variable
	if err := godotenv.Load("./.env"); err != nil {
		log.Println("No .env file found")
	}

	cfg := &Config{
		Addr:   getVarEnv("ADDR", "8080"),
		DB:     getVarEnv("DB", "mongo"),
		DbName: getVarEnv("DB_NAME", ""),
		DbUser: getVarEnv("DB_USER", ""),
		DbPass: getVarEnv("DB_PASS", ""),
		DbHost: getVarEnv("DB_HOST", ""),
		DbPort: getVarEnv("DB_PORT", "5432"),
	}
	return cfg, nil
}

// Simple helper function to read an environment variable or return a default value
func getVarEnv(key string, defaultVal string) string {
	if value, exists := os.LookupEnv(key); exists {
		log.Println(key + " = " + value)
		return value
	}
	log.Println("Default " + key + " = " + defaultVal)
	return defaultVal
}

func boolEnv(valEnv string) bool {
	if valEnv == "true" {
		return true
	} else {
		return false
	}
}
