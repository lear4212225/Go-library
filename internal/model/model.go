package model

type Author struct {
	ID        int    `json:"ID,omitempty"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Books     []Book `json:"books,omitempty"`
}

type Book struct {
	ID              int    `json:"ID,omitempty"`
	Title           string `json:"title"`
	Author          Author `json:"author,omitempty"`
	ForeignAuthorID int    `json:"foreignAuthorID"`
}

type User struct {
	ID            int    `json:"ID"`
	UserName      string `json:"username"`
	ForeignBookID *int
}
