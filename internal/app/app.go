package app

import (
	"context"
	"fmt"
	"github.com/brianvoe/gofakeit/v6"
	"gitlab.com/lear4212225/Go-library/internal/controller"
	"gitlab.com/lear4212225/Go-library/internal/model"
	"gitlab.com/lear4212225/Go-library/pkg/config"
	"log"
	"math/rand"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"time"
)

func Run(cfg *config.Config) error {
	app, err := controller.NewController(cfg)
	if err != nil {
		return err
	}

	r, err := controller.NewRoute(app, cfg)
	if err != nil {
		return err
	}

	err = fillAllTable(app)
	if err != nil {
		return err
	}

	srv := &http.Server{
		Addr:    ":" + cfg.Addr,
		Handler: r,
	}

	// Запуск веб-сервера в отдельном горутине
	go func() {
		log.Println(fmt.Sprintf("server started on port %s ", cfg.Addr))
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()

	// Ожидание сигнала для начала завершения работы
	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt)
	<-quit
	log.Println("Shutdown Server ...")

	// Установка тайм-аута для завершения работы
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		return err
	}

	log.Println("Server exiting")
	return nil
}

func fillAllTable(app *controller.Controller) error {
	gofakeit.Seed(0)
	authors, err := app.Service.ShowAllAuthors()
	if err != nil {
		return err
	}
	if len(authors) == 0 {
		for i := 0; i < 10; i++ {
			a := &model.Author{
				FirstName: gofakeit.FirstName(),
				LastName:  gofakeit.LastName(),
			}
			err = app.Service.AddAuthor(a)
			if err != nil {
				return err
			}
		}

	}

	books, err := app.Service.ShowBooks()
	if len(books) == 0 {

		if authors, err = app.Service.ShowAllAuthors(); err != nil {
			return err
		}
		for i := 0; i < 100; i++ {
			a := &model.Book{
				Title:           gofakeit.BookTitle() + strconv.Itoa(rand.Intn(99999)),
				ForeignAuthorID: authors[i%10].ID,
			}
			err = app.Service.AddBook(a)
			if err != nil {
				return err
			}
		}

	}

	usersAndBooks, err := app.Service.ShowAllUsers()
	if err != nil {
		return err
	}
	if len(usersAndBooks.Users) == 0 {
		for i := 0; i < 50; i++ {
			u := &model.User{
				UserName: gofakeit.Username(),
			}
			err = app.Service.AddUser(u)
			if err != nil {
				return err
			}
		}
	}
	return nil
}
