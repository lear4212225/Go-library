package service

import (
	"errors"
	"gitlab.com/lear4212225/Go-library/internal/model"
	"gitlab.com/lear4212225/Go-library/internal/usecase/repo"
	"gitlab.com/lear4212225/Go-library/pkg/config"
)

type Service struct {
	repo *repo.Repository
}

func NewService(cfg *config.Config) (*Service, error) {
	r, err := repo.NewRepository(cfg)
	if err != nil {
		return nil, err
	}

	s := &Service{repo: r}
	return s, nil

}

type UserAndRentedBooks struct {
	Users       []model.User
	RentedBooks []model.Book
}

func (s *Service) ShowAllUsers() (*UserAndRentedBooks, error) {
	users, err := s.repo.ShowAllUsers()
	if err != nil {
		return nil, err
	}
	var books []model.Book
	for _, user := range users {
		if user.ForeignBookID != nil {
			book, err := s.repo.GetBookByID(*user.ForeignBookID)
			if err != nil {
				return nil, err
			}
			books = append(books, *book)
		}

	}

	authors, err := s.repo.ShowAllAuthors()
	if err != nil {
		return nil, err
	}
	for _, a := range authors {
		for i, _ := range books {
			if a.ID == books[i].ForeignAuthorID {
				books[i].Author = a
			}
		}
	}

	res := &UserAndRentedBooks{
		Users:       users,
		RentedBooks: books}

	return res, nil
}

func (s *Service) AddAuthor(a *model.Author) error {
	var err error
	authors, err := s.repo.ShowAllAuthors()
	if err != nil {
		return err
	}
	for _, author := range authors {
		if author.FirstName == a.FirstName && a.LastName == a.LastName {
			err = errors.New("exist author")
		}
	}
	if err != nil {
		return err
	}
	return s.repo.InsertAuthor(a)

}

func (s *Service) ShowAllAuthors() ([]model.Author, error) {
	authors, err := s.repo.ShowAllAuthors()
	if err != nil {
		return nil, err
	}

	books, err := s.repo.ShowAllBooks()
	if err != nil {
		return nil, err
	}

	for i, a := range authors {
		for _, b := range books {
			if a.ID == b.ForeignAuthorID {
				authors[i].Books = append(authors[i].Books, b)
			}
		}
	}

	return authors, nil

}

func (s *Service) AddBook(book *model.Book) error {
	_, err := s.repo.GetAuthorByID(book.ForeignAuthorID)
	if err != nil {
		return err
	}

	return s.repo.InsertBook(book)

}

func (s *Service) ShowBooks() ([]model.Book, error) {
	books, err := s.repo.ShowAllBooks()

	authors, err := s.repo.ShowAllAuthors()
	if err != nil {
		return nil, err
	}
	for _, a := range authors {
		for i, _ := range books {
			if a.ID == books[i].ForeignAuthorID {
				books[i].Author = a
			}
		}
	}

	return books, nil
}

func (s *Service) GiveBookForUser(idUser, idBook int) error {
	user, err := s.repo.GetUserByID(idUser)
	if err != nil {
		return err
	}
	if user.ForeignBookID != nil {
		return errors.New("user rent book")
	}
	user.ForeignBookID = &idBook
	err = s.repo.UpdateUser(user)
	if err != nil {
		return err
	}

	return nil

}

func (s *Service) GiveBackRentBook(idUser int) error {
	return s.repo.GiveBackRentBook(idUser)
}

func (s *Service) AddUser(user *model.User) error {
	return s.repo.AddUser(user)
}
