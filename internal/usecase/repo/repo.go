package repo

import (
	"context"
	"github.com/jackc/pgx/v5/pgxpool"
	"gitlab.com/lear4212225/Go-library/pkg/config"
	"gitlab.com/lear4212225/Go-library/pkg/postgres"
)

type Repository struct {
	db *pgxpool.Pool
}

func NewRepository(cfg *config.Config) (*Repository, error) {
	conn, err := postgres.DTBConn(cfg)
	if err != nil {
		return nil, err
	}

	err = migration(conn)
	if err != nil {
		return nil, err
	}

	r := &Repository{db: conn}

	return r, err
}

func migration(conn *pgxpool.Pool) error {
	sсhemaAuthors := `
		CREATE TABLE IF NOT EXISTS AUTHORS(
		ID   INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
		FIRST_NAME VARCHAR(40)	NOT NULL,
		LAST_NAME VARCHAR(40)	NOT NULL
		);`

	_, err := conn.Exec(context.Background(), sсhemaAuthors)
	if err != nil {
		return err
	}

	sсhemaBooks := `
		CREATE TABLE IF NOT EXISTS BOOKS(
		ID   INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
		TITLE VARCHAR(70)	NOT NULL UNIQUE,
		AUTHOR_ID INT REFERENCES authors
		);`

	_, err = conn.Exec(context.Background(), sсhemaBooks)
	if err != nil {
		return err
	}

	sсhemaUsers := `
		CREATE TABLE IF NOT EXISTS USERS(
        ID          INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
        USERNAME    VARCHAR(30) NOT NULL UNIQUE,
        BOOK_ID INT REFERENCES books DEFAULT NULL
    );`

	_, err = conn.Exec(context.Background(), sсhemaUsers)
	if err != nil {
		return err
	}

	return nil
}
