package repo

import (
	"context"
	"database/sql"
	"errors"
	"gitlab.com/lear4212225/Go-library/internal/model"
)

var ErrNotExist = errors.New("not exist")

func (r *Repository) GetAuthorByID(id int) (*model.Author, error) {
	row := r.db.QueryRow(context.Background(),
		"SELECT * FROM authors WHERE ID = $1", id)

	var author model.Author
	if err := row.Scan(&author.ID, &author.FirstName, &author.LastName); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, ErrNotExist
		}
		return nil, err
	}
	return &author, nil
}

func (r *Repository) GetUserByID(id int) (*model.User, error) {
	row := r.db.QueryRow(context.Background(),
		"SELECT * FROM users WHERE ID = $1", id)

	var user model.User
	if err := row.Scan(&user.ID, &user.UserName, &user.ForeignBookID); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, ErrNotExist
		}
		return nil, err
	}
	return &user, nil
}

func (r *Repository) GetBookByID(id int) (*model.Book, error) {
	row := r.db.QueryRow(context.Background(),
		"SELECT * FROM books WHERE ID = $1", id)

	var book model.Book
	if err := row.Scan(&book.ID, &book.Title, &book.ForeignAuthorID); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, ErrNotExist
		}
		return nil, err
	}
	return &book, nil
}

func (r *Repository) InsertAuthor(u *model.Author) error {
	// Executing SQL query for insertion
	_, err := r.db.Exec(context.Background(),
		"INSERT INTO authors(FIRST_NAME, LAST_NAME) VALUES($1, $2)", u.FirstName, u.LastName)
	if err != nil {
		return err
	}
	return nil
}

func (r *Repository) InsertBook(b *model.Book) error {
	// Executing SQL query for insertion
	_, err := r.db.Exec(context.Background(),
		"INSERT INTO books(TITLE, AUTHOR_ID) VALUES($1, $2)", b.Title, b.ForeignAuthorID)
	if err != nil {
		return err
	}

	return nil
}

func (r *Repository) InsertUser(u *model.User) error {
	// Executing SQL query for insertion
	_, err := r.db.Exec(context.Background(),
		"INSERT INTO users(USERNAME) VALUES($1)", u.UserName)
	if err != nil {
		return err
	}
	return nil
}

func (r *Repository) UpdateUser(u *model.User) error {
	_, err := r.db.Exec(context.Background(),
		"UPDATE users SET username = $1, book_id = $2 WHERE id = $3",
		u.UserName, u.ForeignBookID, u.ID)
	if err != nil {
		return err
	}

	return nil
}

func (r *Repository) ShowAllAuthors() ([]model.Author, error) {
	rows, err := r.db.Query(context.Background(),
		"SELECT * FROM authors")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var all []model.Author
	for rows.Next() {
		var a model.Author
		if err := rows.Scan(&a.ID, &a.FirstName, &a.LastName); err != nil {
			return nil, err
		}
		all = append(all, a)
	}
	return all, nil
}

func (r *Repository) ShowAllBooks() ([]model.Book, error) {
	rows, err := r.db.Query(context.Background(),
		"SELECT * FROM books")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var all []model.Book
	for rows.Next() {
		var a model.Book
		if err := rows.Scan(&a.ID, &a.Title, &a.ForeignAuthorID); err != nil {
			return nil, err
		}
		all = append(all, a)
	}
	return all, nil
}

func (r *Repository) ShowAllUsers() ([]model.User, error) {
	rows, err := r.db.Query(context.Background(),
		"SELECT * FROM users")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var all []model.User
	for rows.Next() {
		var u model.User
		if err := rows.Scan(&u.ID, &u.UserName, &u.ForeignBookID); err != nil {
			return nil, err
		}
		all = append(all, u)
	}
	return all, nil
}

func (r *Repository) GiveBackRentBook(idUser int) error {
	_, err := r.db.Exec(context.Background(),
		"UPDATE users SET book_id = NULL WHERE id = $1", idUser)
	if err != nil {
		return err
	}
	return nil
}

func (r *Repository) AddUser(user *model.User) error {
	// Executing SQL query for insertion
	_, err := r.db.Exec(context.Background(),
		"INSERT INTO users(USERNAME) VALUES($1)", user.UserName)
	if err != nil {
		return err
	}
	return nil
}
