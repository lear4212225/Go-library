package controller

import (
	"encoding/json"
	"gitlab.com/lear4212225/Go-library/internal/model"
	"io"
	"log"
	"net/http"
	"strconv"
)

func (c *Controller) ShowUsers(w http.ResponseWriter, r *http.Request) {
	// GET
	users, err := c.Service.ShowAllUsers()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	w.WriteHeader(http.StatusOK)
	err = json.NewEncoder(w).Encode(users)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

// articleID := chi.URLParam(r, "UserID")
func (c *Controller) AddAuthor(w http.ResponseWriter, r *http.Request) {
	//  POST
	data, err := io.ReadAll(r.Body)
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	author := &model.Author{}
	json.Unmarshal(data, author)
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	err = c.Service.AddAuthor(author)
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)

}

func (c *Controller) ShowAuthors(w http.ResponseWriter, r *http.Request) {
	// GET
	authors, err := c.Service.ShowAllAuthors()
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	w.WriteHeader(http.StatusOK)
	err = json.NewEncoder(w).Encode(authors)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

}

func (c *Controller) AddBook(w http.ResponseWriter, r *http.Request) {
	//  POST	add book for exist author
	data, err := io.ReadAll(r.Body)
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	book := &model.Book{}
	err = json.Unmarshal(data, book)
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	err = c.Service.AddBook(book)
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	w.WriteHeader(http.StatusOK)
}

func (c *Controller) ShowBooks(w http.ResponseWriter, r *http.Request) {
	// GET
	books, err := c.Service.ShowBooks()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	w.WriteHeader(http.StatusOK)
	err = json.NewEncoder(w).Encode(books)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

}

func (c *Controller) GiveBookForUser(w http.ResponseWriter, r *http.Request) {
	//  id user and id book
	userIDRaw := r.FormValue("userID")
	if userIDRaw == "" {
		http.Error(w, "not user id", http.StatusBadRequest)
	}

	bookIDRaw := r.FormValue("bookID")
	if bookIDRaw == "" {
		http.Error(w, "not book id", http.StatusBadRequest)
	}
	idUser, err := strconv.Atoi(userIDRaw)
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	idBook, err := strconv.Atoi(bookIDRaw)
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = c.Service.GiveBookForUser(idUser, idBook)
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	w.WriteHeader(http.StatusOK)
}

func (c *Controller) GiveBackBook(w http.ResponseWriter, r *http.Request) {
	//  id user
	userIDRaw := r.FormValue("userID")
	if userIDRaw == "" {
		http.Error(w, "not user id", http.StatusBadRequest)
	}

	idUser, err := strconv.Atoi(userIDRaw)
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = c.Service.GiveBackRentBook(idUser)
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	w.WriteHeader(http.StatusOK)
}
