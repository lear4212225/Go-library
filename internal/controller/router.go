package controller

import (
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"gitlab.com/lear4212225/Go-library/internal/usecase/service"
	"gitlab.com/lear4212225/Go-library/pkg/config"
	"net/http"
)

type Controller struct {
	Service *service.Service
}

func NewController(cfg *config.Config) (*Controller, error) {
	serv, err := service.NewService(cfg)
	if err != nil {
		return nil, err
	}
	return &Controller{Service: serv}, nil
}

func NewRoute(app *Controller, cfg *config.Config) (*chi.Mux, error) {
	r := chi.NewRouter()
	r.Use(middleware.Logger)

	//  Author
	r.Route("/author", func(r chi.Router) {
		r.Get("/show-all", app.ShowAuthors)
		r.Post("/add", app.AddAuthor)
	})

	r.Route("/book", func(r chi.Router) {
		r.Get("/show", app.ShowBooks)
		r.Post("/add", app.AddBook)
	})

	r.Route("/user", func(r chi.Router) {
		r.Get("/show", app.ShowUsers)
		r.Post("/give-book", app.GiveBookForUser)
		r.Post("/give-back-book", app.GiveBackBook)

	})

	//SwaggerUI
	r.Get("/swagger", swaggerUI)
	r.Get("/public/*", func(w http.ResponseWriter, r *http.Request) {
		http.StripPrefix("/public/", http.FileServer(http.Dir("./public"))).ServeHTTP(w, r)
	})

	return r, nil
}
